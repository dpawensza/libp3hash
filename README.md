# libP3Hash
Library for handling files encrypted by Patapon 3 security measures.
Fork with removed `main()` function for use as a library.  

Made by Owocek (c) 2019  

Fork base: https://github.com/efonte/libP3Hash
